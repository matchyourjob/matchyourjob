MatchYourJob

Rappels des enjeux du projet
L'objectif de cette application est de créer une application du style Tinder mais adapté au monde du travail, donc avec des chercheurs d'emplois et des offres d'emploi poster par des entreprises.

Rendu
La date limite de rendu du projet est le vendredi 01 juillet 2022 à 17h15

Rôle des développeurs dans l'équipe
Le projet étant à rendre sous 3 jours, nous répartissons les rôles de façon à entamer rapidement le projet, la rédaction du README est faite en parallèle avec le début du développement.

Architecture et technologies du projet (lib + versions)

Le projet est réalisé en HTML 5, JavaScript 11, CSS 3, PHP 7 et avec le framework Bootstrap 5.

On utilisera Sonarcloud à travers une pipeline GitLab pour assurer en continu la bonne qualité du code.

On utilisera Figma pour la création des maquettes

On utilisera Trello pour la planification des tâches    

Risques humains : - Le projet pourrait ralentir fortement en l'absence de l'un ou de plusieurs membres du groupe

Planification des tâches
Voici le workflow utilisé sur le projet :

A faire
En cours
Fini

https://trello.com/b/GqvVoeG9/hackathon-match-your-job-myj

Convention de nommage des commits
9 types de commit sont disponibles :

build : changements qui affectent le système de build ou des dépendances externes (npm, make…)
ci : changements concernant les fichiers et scripts d’intégration ou de configuration (Travis, Ansible, BrowserStack…)
feat : ajout d’une nouvelle fonctionnalité
wip : avancement sur une fonctionnalité
fix : correction d’un bug
perf : amélioration des performances
refactor : modification qui n’apporte ni nouvelle fonctionalité ni d’amélioration de performances
style : changement qui n’apporte aucune alteration fonctionnelle ou sémantique (indentation, mise en forme, ajout d’espace, renommante d’une variable…)
docs : rédaction ou mise à jour de documentation
test : ajout ou modification de tests
Penser à préciser le numéro du ticket quand cela est pertinent.

Bonnes pratiques de codage à respecter
Le projet étant principalement écrit en JS, les développeurs suivront la normes de propreté du code ES6 https://fr.wikipedia.org/wiki/ECMAScript. Quelques rappels :

N’utiliser pas les commentaires HTML dans le bloc script
Placez les scripts au bas de votre page
Utilisez === au lieu de ==
Déclarer les variables à l’extérieur des boucles
Utiliser {} au lieu de New Object()
Utilisez [] au lieu de New Array()
Supprimer l’attribut obsolète « language » dans la balise script
Pensez aux paramètres par défaut dans vos fonctions et méthodes
Utilisez le template string à la place des concaténation (`${}`)
Il est attendu que chaque développeur commente son code de façon claire et pertinente.

Branches
Le projet étant court et l'équipe composée de 3 personnes uniquement, nous travaillerons uniquement sur 2 branches :

Une branche dev, branche de développement sur laquelle les développeurs pousseront leurs codes en respectant la convention de nommage des commits.
Une branche main, sur laquelle aucun code ne doit être poussé. Le code présent sur cette branche doit constamment avoir été vérifié par le maintainer du projet à l'étape du merge request provenant de la branche dev.
Une branche feat/PHP, sur laquelle on mettra toutes les features liées au php
Tests et déploiements
Utilisation de Sonarcloud via une pipeline exécutée automatiquement sur les branches main et dev.

Statut du projet
Si vous manquez d'énergie ou de temps pour le projet, placez une note en haut de ce README indiquant que le développement a ralenti ou s'est complètement arrêté. Quelqu'un pourra choisir de fork le projet ou de se porter volontaire pour intervenir en tant que mainteneur ou propriétaire, permettant au projet de continuer. Vous pouvez également faire une demande explicite pour les responsables.
